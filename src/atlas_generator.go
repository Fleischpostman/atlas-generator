package main

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	"strings"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	directory = kingpin.Flag("directory",
		"directory containing the sprites that will be merged into an atlas").
		PlaceHolder("DIRECTORY").Default("").String()
)

func exitProgram(errorMessage string, exitCode int) {
	fmt.Println(errorMessage + " Exiting program ...")
	os.Exit(exitCode)
}

func main() {
	kingpin.Parse()

	if *directory == "" {
		exitProgram("No directory specified.", 1)
	} else {
		files, err := ioutil.ReadDir(*directory)

		if err != nil {
			// TODO(Fabian): Diagnostics ...
			exitProgram("Directory could not be read.", 2)
		}

		imageFiles := []os.FileInfo{}
		for _, file := range files {
			if file.IsDir() {
				continue
			}
			var substrings = strings.Split(file.Name(), ".")
			if substrings[len(substrings)-1] == "png" {
				imageFiles = append(imageFiles, file)
			}
		}

		if len(imageFiles) == 0 {
			exitProgram("No image files in specified directory.", 0)
		} else {
			images := make([]image.Image, len(imageFiles))
			for i, file := range imageFiles {
				image, err := os.Open(*directory + "/" + file.Name())
				defer image.Close()

				if err != nil {
					exitProgram("Image could not be opened.", 3)
				}

				images[i], err = png.Decode(image)
				if err != nil {
					exitProgram("Could not load image.", 4)
				}
			}

			requiredWidth := 0
			maxHeight := images[0].Bounds().Max.Y
			for _, image := range images {
				requiredWidth += image.Bounds().Max.X
				if image.Bounds().Max.Y > maxHeight {
					maxHeight = image.Bounds().Max.Y
				}
			}

			fmt.Printf("Max width: %d\n", requiredWidth)
			fmt.Printf("Max height: %d\n", maxHeight)

			atlas := image.NewRGBA(image.Rect(0, 0, requiredWidth, maxHeight))

			startX := 0
			for _, image := range images {
				for y := 0; y < image.Bounds().Max.Y; y++ {
					for x := 0; x < image.Bounds().Max.X; x++ {
						atlas.Set(startX+x, y, image.At(x, y))
					}
				}

				startX += image.Bounds().Max.X
			}

			atlasFile, err := os.Create("atlas.png")
			if err != nil {
				// Handle error
			}
			png.Encode(atlasFile, atlas)

		}
	}
}
